import DriverPantalla as TFT
import RPi.GPIO as GPIO
import getIP as IP
import libreria_ad as AD
from sys import getsizeof
import time
import serial
import pinta as Pinta
import utils as Utils

I = ['83C0', '83C0', '83C0', '83C0', '83C0', '83C0', '83C0', '83C0', '83C0', '83C0', '83C0', '83C0', '83C0', '83C0']
P = ['7FF8', '7FFC', '780E', '780E', '780E', '780E', '7FFC', '7FF8', '7800', '7800', '7800', '7800', '7800', '7800']
Negre = 0x0000
Blau = 0x00FF
Vermell = 0xF000
Blanc = 0xFFFF
BarraC = 0x0F00

Escala = [65144, 65264, 64784, 63464, 65384, 64904, 64424, 64304, 63824, 63944]
#Tactil = []
blocX=22
blocY=62
potencia = 0
BarraC = 0xFFE0
esc = 0

Trigger = 11
Echo = 12
xcoord = 16
ycoord = 15
zcoord = 13

y = 100 #Fila
x = 230 #Columna
TFT.Config_Pins()
GPIO.setwarnings(False)
GPIO.setup(Trigger,GPIO.OUT)
GPIO.setup(Echo,GPIO.IN)
GPIO.setup(xcoord,GPIO.IN)
GPIO.setup(ycoord,GPIO.IN)
GPIO.setup(zcoord,GPIO.IN)
TFT.SPI_TFT_Reset()
ser = serial.Serial('/dev/ttyACM0', 9600)#, timeout=1)
ser.open()
#Pinta.Pantalla(Blanc)
Pinta.IP(IP.getIP(), x, y)
Pinta.Rectangle(20, 60, 25, 201, Negre) #Barra
Pinta.Rectangle(32, 20, 0, 20, Vermell) #Menys
Pinta.Rectangle(32, 280, 0, 20, Vermell) #Mes
Pinta.Rectangle(22, 290, 20, 0, Vermell) #Mes
Pinta.Rectangle(80, 20, 150, 279, Vermell) #Tactil
Pinta.Rectangle(55, 100, 22, 40, Blau) #Clean
Pinta.Rectangle(55, 150, 22, 40, Blau) #TextMode
Utils.escriuFrase("Clean", 240-70, 103)
Utils.escriuFrase("Text", 240-70, 156) 	

while (True):
	#Pinta.IP(IP.getIP(), 230, 100)
	x = AD.leer_X1()
	y = AD.leer_Y()
	#if (x > 0 and y > 0): 
	x = 220-(x-450)/14
	y = (y-420)/11
	x = int(x*(14.58/14))
	y = int(y*(10.93/10))
	if (Utils.haPremut(x, y)):
		k = Utils.velocitatMotor(x, y)
		if (Utils.volPintar(x, y)):
			Pinta.pinta(ord('.'), x, y)
		elif (Utils.clean(x, y)):
			Pinta.netejaTactil()
		elif (Utils.textMode(x, y)):
			Pinta.netejaTactil()
			Pinta.netejaText()
			Pinta.modeText()
		elif (k == 1 and blocY > 80):
			potencia = potencia - 25
			ser.write(str(potencia))
			blocY = blocY - 3			
			Utils.borraBloc(blocX, blocY)
			esc = esc - 1
			blocY = blocY - 17			
			time.sleep(1)
		elif (k == 2 and blocY < 245):
			potencia = potencia + 25
			ser.write(str(potencia))
			Utils.dibuixaBloc(blocX, blocY, Escala[esc])
			esc = esc + 1
			BarraC = BarraC - (0x0FE0/100)*3
			blocY = blocY + 20
			time.sleep(1)
	"""GPIO.output(Trigger,True)
	time.sleep(0.00001)
	GPIO.output(Trigger,False)
	#while (GPIO.input(Echo) == 0):
		i =0
	t = time.time()
	while (GPIO.input(Echo)):
		i =0
	t = time.time() - t
	#print (t * 1000000)/58"""
	
	

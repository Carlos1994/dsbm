#libreria analog tactil

#FIB, DSBM, Enric X. Martin Rull, March 2016

import DriverPantalla as TFT
import RPi.GPIO as GPIO
import time as Time

#Connections, TFT pin = board pin

#CS_Pin    =  12
#Reset_Pin =  22
#SCLK_Pin  =  23
#SDO_Pin   =  21
#SDI_Pin   =  19

global char_x
global char_y
global font
#Connections AD pin
Y1 = 36		#pin para el eje (Y+)
X1 = 35 	#pin para el eje (X+)
Y0 = 40  	#pin para el eje (Y-)
X0 = 38		#pin para el eje (X-)

def preparar_lect_X():
 GPIO.setup(Y1,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
 GPIO.setup(X1,GPIO.OUT)
 GPIO.setup(Y0,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
 GPIO.setup(X0,GPIO.OUT)
 GPIO.output(X1,False)
 GPIO.output(X0,True)

#pag 19/40 manual
def leer_X():
 preparar_lect_X()
 TFT.CS_TFT(False, 0)
 Time.sleep(0.025)
 TFT.Send_SPI(True, 0) #start
 TFT.Send_SPI(True, 0) #single/!diff
 # CH0
 TFT.Send_SPI(False, 0) #D2
 TFT.Send_SPI(False, 0) #D1 
 TFT.Send_SPI(False, 0) #D0

 TFT.Send_SPI(True, 0) #X
 #Rebem les dades del canal CH0 -> X
 TFT.Recv_SPI(0) #X
 value = 0
 TFT.Recv_SPI(0)
 value = value | (TFT.Recv_SPI(0) << 11) #B11
 TFT.Recv_SPI(0)
 value = value | (TFT.Recv_SPI(0) << 10) #B10
 TFT.Recv_SPI(0)
 value = value | (TFT.Recv_SPI(0) << 9) #B9
 TFT.Recv_SPI(0)
 value = value | (TFT.Recv_SPI(0) << 8) #B8
 TFT.Recv_SPI(0)
 value = value | (TFT.Recv_SPI(0) << 7) #B7
 TFT.Recv_SPI(0)
 value = value | (TFT.Recv_SPI(0) << 6) #B6
 TFT.Recv_SPI(0)
 value = value | (TFT.Recv_SPI(0) << 5) #B5
 TFT.Recv_SPI(0)
 value = value | (TFT.Recv_SPI(0) << 4) #B4
 TFT.Recv_SPI(0)
 value = value | (TFT.Recv_SPI(0) << 3) #B3
 TFT.Recv_SPI(0)
 value = value | (TFT.Recv_SPI(0) << 2) #B2
 TFT.Recv_SPI(0)
 value = value | (TFT.Recv_SPI(0) << 1) #B1
 TFT.Recv_SPI(0)
 value = value | (TFT.Recv_SPI(0) << 0) #B0
 TFT.CS_TFT(True, 0)
 return value

def preparar_lect_Y():
 GPIO.setup(Y1,GPIO.OUT)
 GPIO.setup(X1,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
 GPIO.setup(Y0,GPIO.OUT)
 GPIO.setup(X0,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
 GPIO.output(Y1,True)
 GPIO.output(Y0,False)


def leer_Y():
 preparar_lect_Y()
 TFT.CS_TFT(False, 0)
 Time.sleep(0.000001)
 TFT.Send_SPI(True, 0) #start
 TFT.Send_SPI(True, 0) #single/!diff
 # CH1
 TFT.Send_SPI(False, 0) #D2
 TFT.Send_SPI(True, 0) #D1 
 TFT.Send_SPI(False, 0) #D0

 TFT.Send_SPI(True, 0) #X
 #Rebem les dades del canal CH1 -> Y
 TFT.Recv_SPI(0) #X
 value = 0
 value = value | (TFT.Recv_SPI(0) << 11) #B11
 value = value | (TFT.Recv_SPI(0) << 10) #B10
 value = value | (TFT.Recv_SPI(0) << 9) #B9
 value = value | (TFT.Recv_SPI(0) << 8) #B8
 value = value | (TFT.Recv_SPI(0) << 7) #B7
 value = value | (TFT.Recv_SPI(0) << 6) #B6
 value = value | (TFT.Recv_SPI(0) << 5) #B5
 value = value | (TFT.Recv_SPI(0) << 4) #B4
 value = value | (TFT.Recv_SPI(0) << 3) #B3
 value = value | (TFT.Recv_SPI(0) << 2) #B2
 value = value | (TFT.Recv_SPI(0) << 1) #B1
 value = value | (TFT.Recv_SPI(0) << 0) #B0
 TFT.CS_TFT(True, 0)
 return value

def leer_X1():
 preparar_lect_X()
 TFT.CS_TFT(False, 0)
 Time.sleep(0.000001)
 TFT.Send_SPI(True, 0) #start
 TFT.Send_SPI(True, 0) #single/!diff
 # CH0
 TFT.Send_SPI(False, 0) #D2
 TFT.Send_SPI(False, 0) #D1 
 TFT.Send_SPI(True, 0) #D0

 TFT.Send_SPI(True, 0) #X
 #Rebem les dades del canal CH0 -> X
 TFT.Recv_SPI(0) #X
 value = 0
 m = TFT.Recv_SPI(0)
 value = value | (m << 11) #B11
 m = TFT.Recv_SPI(0)
 value = value | (m << 10) #B10
 m = TFT.Recv_SPI(0)
 value = value | (m << 9) #B9
 m = TFT.Recv_SPI(0)
 value = value | (m << 8) #B8
 m = TFT.Recv_SPI(0)
 value = value | (m << 7) #B7
 m = TFT.Recv_SPI(0)
 value = value | (m << 6) #B6
 m = TFT.Recv_SPI(0)
 value = value | (m << 5) #B5
 m = TFT.Recv_SPI(0)
 value = value | (m << 4) #B4
 m = TFT.Recv_SPI(0)
 value = value | (m << 3) #B3 
 m = TFT.Recv_SPI(0)
 value = value | (m << 2) #B2
 m = TFT.Recv_SPI(0)
 value = value | (m << 1) #B1 
 m = TFT.Recv_SPI(0)
 value = value | (m << 0) #B0
 TFT.CS_TFT(True, 0)
 return value

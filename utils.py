import DriverPantalla as TFT
import pinta as Pinta

Negre = 0x0000
Blau = 0x00FF
Vermell = 0xF000
Blanc = 0xFFFF
Groc = 0xFFE0

def haPremut(x, y):
	if (x >= 0 and x < 240 and y >= 0 and y < 320):
		return 1
	return 0

def volPintar(x, y):
	if ((240-x) > 84 and (240-x) < 231 and y > 20 and y < 297):
		return 1
	return 0

def textMode(x, y):
	if (x > 160 and x < 180 and y > 150 and y < 190):	
		print "Text"		
		return 1
	return 0

def clean(x, y):
	if (x > 160 and x < 180 and y > 100 and y < 140):
		print "Clean"	
		return 1
	return 0

def velocitatMotor(x, y):
	if (x > 185 and x < 210 and y > 20 and y < 50):
		return 1
	elif (x > 185 and x < 210 and y > 270 and y < 300):
		return 2
	return 0

def borraBloc(blocX, blocY):
	for i in range(22):
		for j in range(18):
			TFT.SPI_TFT_pixel(240-blocX-i,blocY-j,Blanc)

def dibuixaBloc(blocX, blocY, BarraC):
	for i in range(22):
		for j in range(18):
			TFT.SPI_TFT_pixel(240-blocX-i,blocY+j, BarraC)

def escriuFrase(frase, x, y):
	for i in range(len(frase)):
		k = frase[i]
		Pinta.pinta2(ord(k), x, y)
		y = y + 7
